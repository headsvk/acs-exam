#!/bin/bash

echo "Threads,Success Ratio,Customer Interaction Ratio,Throughput (successful customer interactions per second),Latency (seconds)"

for i in `seq 1 30`;
    do
    for x in `seq 1 1`;
        do
            java -jar out/artifacts/acertaininventorymanager_jar/acertaininventorymanager.jar -local=true -t=$i -table=true
        done
    done

