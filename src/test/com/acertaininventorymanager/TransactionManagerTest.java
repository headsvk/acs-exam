package com.acertaininventorymanager;

import com.acertaininventorymanager.business.*;
import com.acertaininventorymanager.client.TransactionManagerHTTPProxy;
import com.acertaininventorymanager.exceptions.EmptyRegionException;
import com.acertaininventorymanager.interfaces.CustomerTransactionManager;
import com.acertaininventorymanager.interfaces.ItemDataManager;
import com.acertaininventorymanager.exceptions.InexistentCustomerException;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.exceptions.NonPositiveIntegerException;
import com.acertaininventorymanager.mock.InventoryManagerMock;
import com.acertaininventorymanager.utils.InventoryManagerConstants;
import org.junit.*;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class TransactionManagerTest {

    private static final int LOCAL_DATA_MANAGERS_COUNT = 3;

    private static final int NUM_ITERATIONS = 100;

    private static final boolean LOCAL_TEST = false;

    private static CustomerTransactionManager sTransactionManager;

    // List of customers sorted by id
    private static List<Customer> sCustomers;

    private static InventoryManagerMock sMock = InventoryManagerMock.getInstance();
    
    private Random mRandom = new Random();

    @BeforeClass
    public static void setup() throws Exception {

        sCustomers = sMock.getCustomers().stream()
                .sorted(Comparator.comparingInt(Customer::getCustomerId))
                .collect(Collectors.toList());

        String localTestProperty = System.getProperty(InventoryManagerConstants.PROPERTY_KEY_LOCAL_TEST);
        boolean localTest = (localTestProperty != null) ? Boolean.parseBoolean(localTestProperty) : LOCAL_TEST;

        if (localTest) {

            List<ItemDataManager> dataManagers = new ArrayList<>();
            for (int i = 0; i < LOCAL_DATA_MANAGERS_COUNT; i++) {
                dataManagers.add(new CertainDataManager());
            }
            sTransactionManager = new CertainTransactionManager(dataManagers, sMock.getCustomers());

        } else {
            sTransactionManager = new TransactionManagerHTTPProxy("http://localhost:8081");
        }
    }

    @AfterClass
    public static void cleanup() {
        if (sTransactionManager instanceof TransactionManagerHTTPProxy) {
            ((TransactionManagerHTTPProxy) sTransactionManager).stop();
        }
    }

    @Before
    public void clearDatabase() throws InventoryManagerException {
        if (sTransactionManager instanceof CertainTransactionManager) {
            ((CertainTransactionManager) sTransactionManager).clearValuesBought();
        } else if (sTransactionManager instanceof TransactionManagerHTTPProxy) {
            ((TransactionManagerHTTPProxy) sTransactionManager).clearValuesBought();
        }
    }

    @Test(expected = NonPositiveIntegerException.class)
    public void testProcessInvalidOrderId() throws InventoryManagerException {

        Set<ItemPurchase> purchases = sMock.generateValidPurchases(20);

        // add a purchase with invalid order id
        purchases.add(new ItemPurchase(mRandom.nextInt(1000) - 1001,
                sMock.generateCustomerId(), sMock.generateItemId(), sMock.generateQuantity(), sMock.generateUnitPrice()));

        sTransactionManager.processOrders(purchases);   // should fail
    }

    @Test(expected = NonPositiveIntegerException.class)
    public void testProcessInvalidQuantity() throws InventoryManagerException {

        Set<ItemPurchase> purchases = sMock.generateValidPurchases(20);

        // add a purchase with invalid quantity
        purchases.add(new ItemPurchase(200, sMock.generateCustomerId(), sMock.generateItemId(),
                mRandom.nextInt(1000) - 1001, sMock.generateUnitPrice()));

        sTransactionManager.processOrders(purchases);   // should fail
    }

    @Test(expected = NonPositiveIntegerException.class)
    public void testProcessInvalidUnitPrice() throws InventoryManagerException {

        Set<ItemPurchase> purchases = sMock.generateValidPurchases(20);

        // add a purchase with invalid unit price
        purchases.add(new ItemPurchase(200, sMock.generateCustomerId(), sMock.generateItemId(), sMock.generateQuantity(),
                mRandom.nextInt(1000) - 1001));

        sTransactionManager.processOrders(purchases);   // should fail
    }

    @Test(expected = InexistentCustomerException.class)
    public void testProcessInvalidCustomerId() throws InventoryManagerException {

        Set<ItemPurchase> purchases = sMock.generateValidPurchases(20);

        // add a purchase with invalid customer id
        purchases.add(new ItemPurchase(200, mRandom.nextInt(1000) - 1001,
                sMock.generateItemId(), sMock.generateQuantity(), sMock.generateUnitPrice()));

        sTransactionManager.processOrders(purchases);   // should fail
    }

    @Test
    public void testRandomPurchases() throws InventoryManagerException {

        for (int i = 0; i < 10; i++) {
            sTransactionManager.processOrders(sMock.generateValidPurchases(mRandom.nextInt(1000) + 1));
        }
    }

    @Test(expected = NonPositiveIntegerException.class)
    public void testRegionTotalsInvalidId() throws InventoryManagerException {
        Set<Integer> regionIds = sMock.getRandomRegions();
        regionIds.add(mRandom.nextInt(1000) - 1001);    // invalid region id
        sTransactionManager.getTotalsForRegions(regionIds);
    }

    @Test(expected = EmptyRegionException.class)
    public void testRegionTotalsEmptyRegion() throws InventoryManagerException {
        Set<Integer> regionIds = sMock.getRandomRegions();
        regionIds.add(mRandom.nextInt(sMock.getRegionsCount()) + 1000);    // empty region
        sTransactionManager.getTotalsForRegions(regionIds);
    }

    @Test
    public void testRegionTotals() throws InventoryManagerException {
        // add random purchases
        sTransactionManager.processOrders(sMock.generateValidPurchases(1000));

        // get 5 random customers
        List<Customer> customers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            customers.add(sCustomers.get(sMock.generateCustomerId()));
        }

        // get their regions
        Set<Integer> regionIds = customers.stream().map(Customer::getRegionId).collect(Collectors.toSet());

        // get totals before
        List<RegionTotal> totalsPre = sTransactionManager.getTotalsForRegions(regionIds);
        totalsPre.sort(Comparator.comparingInt(RegionTotal::getRegionId));

        // add purchases
        Set<ItemPurchase> purchases = new HashSet<>();
        Map<Integer, Long> regionPurchases = new HashMap<>();
        int orderId = 2000;
        for (Customer customer : customers) {
            regionPurchases.merge(customer.getRegionId(), 200L, (a, b) -> a + b);   // add 200 to the region total
            purchases.add(new ItemPurchase(orderId++, customer.getCustomerId(), sMock.generateItemId(), 10, 20));
        }
        sTransactionManager.processOrders(purchases);

        // get totals after and check that they were updated
        List<RegionTotal> totalsPost = sTransactionManager.getTotalsForRegions(regionIds);
        totalsPost.sort(Comparator.comparingInt(RegionTotal::getRegionId));
        for (int i = 0; i < totalsPost.size(); i++) {
            RegionTotal regionTotal = totalsPost.get(i);
            Assert.assertEquals(
                    regionPurchases.get(regionTotal.getRegionId()) + totalsPre.get(i).getTotalValueBought(),
                    totalsPost.get(i).getTotalValueBought());
        }
    }

    /**
     * Simulates 3 clients making 100 purchases in a loop and checks region totals in another loop.
     * Each purchase costs 1 so region totals should increase only by 100.
     * <p>
     * There's a 50% chance that there will be one invalid purchase so all other 300 purchases should be cancelled.
     * <p>
     * {@link #NUM_ITERATIONS} specifies number of iterations in both loops.
     */
    @Test
    public void testDirtyReadWrites() throws ExecutionException, InterruptedException, InventoryManagerException {

        Set<ItemPurchase> purchases = new HashSet<>();
        Set<Integer> regionIds = new HashSet<>();

        for (int i = 0; i < 3; i++) {
            // pick random customer
            Customer customer = sCustomers.get(sMock.generateCustomerId());
            regionIds.add(customer.getRegionId());

            // add 100 purchases, each for 1
            for (int id = 1; id <= 100; id++) {
                purchases.add(new ItemPurchase(i * 1000 + id, customer.getCustomerId(),
                        sMock.generateItemId(), 1, 1));
            }
        }

        Set<ItemPurchase> invalidPurchases = new HashSet<>(purchases);
        invalidPurchases.add(new ItemPurchase(1, -1, 1, 1, 1));

        Future<?> future1 = Executors.newSingleThreadExecutor().submit(() -> {

            for (int i = 0; i < NUM_ITERATIONS; i++) {
                try {
                    sTransactionManager.processOrders(mRandom.nextBoolean() ? purchases : invalidPurchases);

                } catch (InventoryManagerException ignored) {
                    // we should get InexistentCustomerException if invalid purchases are sent
                }
            }
        });

        Future<?> future2 = Executors.newSingleThreadExecutor().submit(() -> {

            for (int i = 0; i < NUM_ITERATIONS; i++) {
                try {
                    List<RegionTotal> regionTotals = sTransactionManager.getTotalsForRegions(regionIds);

                    regionTotals.forEach(total ->
                            Assert.assertEquals(0, total.getTotalValueBought() % 100));

                } catch (InventoryManagerException e) {
                    e.printStackTrace();
                }
            }
        });

        future1.get();
        future2.get();

        // check final state
        List<RegionTotal> regionTotals = sTransactionManager.getTotalsForRegions(regionIds);
        regionTotals.forEach(total -> Assert.assertEquals(0, total.getTotalValueBought() % 100));
    }
}
