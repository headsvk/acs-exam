package com.acertaininventorymanager;

import com.acertaininventorymanager.business.CertainDataManager;
import com.acertaininventorymanager.business.CertainTransactionManager;
import com.acertaininventorymanager.business.ItemPurchase;
import com.acertaininventorymanager.business.RegionTotal;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.ItemDataManager;
import com.acertaininventorymanager.mock.InventoryManagerMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TransactionManagerRollbackTest {

    private static class DataManager extends CertainDataManager {

        private int mCounter = 1;

        @Override
        public synchronized void addItemPurchase(ItemPurchase itemPurchase) throws InventoryManagerException {
            if (mCounter++ % 13 == 0) {
                throw new InventoryManagerException("I can't stand 13 items of type " + itemPurchase.getItemId());
            } else {
                super.addItemPurchase(itemPurchase);
            }
        }
    }

    private static final int VALID_AMOUNT = 12;

    private static final int LOCAL_DATA_MANAGERS_COUNT = 3;

    private static InventoryManagerMock sMock = InventoryManagerMock.getInstance();

    private CertainTransactionManager mTransactionManager;

    @Before
    public void setup() {

        List<ItemDataManager> dataManagers = new ArrayList<>();
        for (int i = 0; i < LOCAL_DATA_MANAGERS_COUNT; i++) {
            dataManagers.add(new DataManager());
        }
        mTransactionManager = new CertainTransactionManager(dataManagers, sMock.getCustomers());
        mTransactionManager.clearValuesBought();
    }

    @Test
    public void testAddSmallAmount() throws InventoryManagerException {

        Set<ItemPurchase> purchases = new HashSet<>(VALID_AMOUNT * LOCAL_DATA_MANAGERS_COUNT);
        long totalValueBoughtExpected = 0;

        for (int itemId = 1; itemId <= LOCAL_DATA_MANAGERS_COUNT; itemId++) {
            for (int i = 1; i <= VALID_AMOUNT; i++) {
                ItemPurchase purchase = new ItemPurchase(itemId * 10 + i,
                        sMock.generateCustomerId(), itemId, sMock.generateQuantity(), sMock.generateUnitPrice());
                totalValueBoughtExpected += purchase.getTotalValue();
                purchases.add(purchase);
            }
        }

        mTransactionManager.processOrders(purchases);

        List<RegionTotal> regionTotals = mTransactionManager.getTotalsForRegions(sMock.getAllRegions());
        long totalValueBought = regionTotals.stream().mapToLong(RegionTotal::getTotalValueBought).sum();

        Assert.assertEquals(totalValueBoughtExpected, totalValueBought);
    }


    @Test
    public void testAddBigAmount() throws InventoryManagerException {

        Set<ItemPurchase> purchases = new HashSet<>(VALID_AMOUNT * LOCAL_DATA_MANAGERS_COUNT + 10);

        for (int itemId = 1; itemId <= LOCAL_DATA_MANAGERS_COUNT; itemId++) {
            int amount = itemId == 2 ? VALID_AMOUNT + 10 : VALID_AMOUNT;

            for (int i = 1; i <= amount; i++) {
                ItemPurchase purchase = new ItemPurchase(itemId * 10 + i,
                        sMock.generateCustomerId(), itemId, sMock.generateQuantity(), sMock.generateUnitPrice());
                purchases.add(purchase);
            }
        }

        assertThatExceptionOfType(InventoryManagerException.class)
                .isThrownBy(() -> mTransactionManager.processOrders(purchases))
                .withMessage("I can't stand 13 items of type 2");

        List<RegionTotal> regionTotals = mTransactionManager.getTotalsForRegions(sMock.getAllRegions());
        long totalValueBought = regionTotals.stream().mapToLong(RegionTotal::getTotalValueBought).sum();

        Assert.assertEquals(0, totalValueBought);
    }

}
