package com.acertaininventorymanager.workloads;

import com.acertaininventorymanager.business.CertainDataManager;
import com.acertaininventorymanager.business.CertainTransactionManager;
import com.acertaininventorymanager.client.TransactionManagerHTTPProxy;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.CustomerTransactionManager;
import com.acertaininventorymanager.interfaces.ItemDataManager;
import com.acertaininventorymanager.mock.InventoryManagerMock;
import com.acertaininventorymanager.utils.InventoryManagerConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * CertainWorkload class runs the workloads by different workers concurrently.
 * It configures the environment for the workers using WorkloadConfiguration
 * objects and reports the metrics.
 */
public class CertainWorkload {

    private static final int LOCAL_DATA_MANAGERS_COUNT = 3;

    private static final boolean LOCAL_TEST = true;

    private static final boolean TABLE_MODE = false;

    private static final String TRANSACTION_MANAGER_SERVER_URL = "http://localhost:8081";

    public static void main(String[] args) throws Exception {

        int numConcurrentWorkloadThreads = 10;

        // Initialize the RPC interfaces if its not a localTest, the variable is
        // overriden if the property is set
        String localTestProperty = System
                .getProperty(InventoryManagerConstants.PROPERTY_KEY_LOCAL_TEST);
        boolean localTest = (localTestProperty != null) ? Boolean
                .parseBoolean(localTestProperty) : LOCAL_TEST;

        boolean tableMode = TABLE_MODE;

        // Override parameters with program arguments
        for (String arg : args) {
            if (arg.startsWith("-t=")) {
                numConcurrentWorkloadThreads = Integer.valueOf(arg.substring(3));

            } else if (arg.startsWith("-local=")) {
                localTest = Boolean.valueOf(arg.substring(7));

            } else if (arg.startsWith("-table=")) {
                tableMode = Boolean.valueOf(arg.substring(7));
            }
        }

        List<WorkerRunResult> workerRunResults = new ArrayList<>();
        List<Future<WorkerRunResult>> runResults = new ArrayList<>();

        CustomerTransactionManager transactionManager;

        if (localTest) {

            List<ItemDataManager> dataManagers = new ArrayList<>();
            for (int i = 0; i < LOCAL_DATA_MANAGERS_COUNT; i++) {
                dataManagers.add(new CertainDataManager());
            }
            transactionManager = new CertainTransactionManager(dataManagers, InventoryManagerMock.getInstance().getCustomers());

        } else {
            transactionManager = new TransactionManagerHTTPProxy(TRANSACTION_MANAGER_SERVER_URL);
        }

        // Generate data in the inventory manager before running the workload
        initializeInventoryManagerData(transactionManager);

        ExecutorService exec = Executors
                .newFixedThreadPool(numConcurrentWorkloadThreads);

        for (int i = 0; i < numConcurrentWorkloadThreads; i++) {
            WorkloadConfiguration config = new WorkloadConfiguration(transactionManager);
            Worker workerTask = new Worker(config);
            // Keep the futures to wait for the result from the thread
            runResults.add(exec.submit(workerTask));
        }

        // Get the results from the threads using the futures returned
        for (Future<WorkerRunResult> futureRunResult : runResults) {
            WorkerRunResult runResult = futureRunResult.get(); // blocking call
            workerRunResults.add(runResult);
        }

        exec.shutdownNow(); // shutdown the executor

        // Finished initialization, stop the clients if not localTest
        if (!localTest) {
            ((TransactionManagerHTTPProxy) transactionManager).stop();
        }

        reportMetric(workerRunResults, tableMode, numConcurrentWorkloadThreads);
    }

    /**
     * Computes the metrics and prints them.
     */
    private static void reportMetric(List<WorkerRunResult> workerRunResults, boolean tableMode, int threads) {
        // create an empty accumulator
        WorkerRunResult combinedResult = new WorkerRunResult(0, 0, 0, 0, 0);

        // aggregate all metrics
        for (WorkerRunResult result : workerRunResults) {
            combinedResult.setTotalRuns(combinedResult.getTotalRuns() + result.getTotalRuns());

            combinedResult.setElapsedTimeInNanoSecs(
                    combinedResult.getElapsedTimeInNanoSecs() + result.getElapsedTimeInNanoSecs());

            combinedResult.setSuccessfulCustomerInteractions(
                    combinedResult.getSuccessfulCustomerInteractions()
                            + result.getSuccessfulCustomerInteractions());

            combinedResult.setSuccessfulInteractions(combinedResult.getSuccessfulInteractions()
                    + result.getSuccessfulInteractions());

            combinedResult.setTotalCustomerInteractions(
                    combinedResult.getTotalCustomerInteractions()
                            + result.getTotalCustomerInteractions());
        }

        double elapsedTimeSeconds = (double) combinedResult.getElapsedTimeInNanoSecs() / 1E9;

        double successRatio = combinedResult.getSuccessfulCustomerInteractions() /
                (double) combinedResult.getTotalCustomerInteractions();
        double customerRatio = combinedResult.getTotalCustomerInteractions() /
                (double) combinedResult.getTotalRuns();
        double throughput = combinedResult.getSuccessfulCustomerInteractions() / elapsedTimeSeconds;
        double latency = elapsedTimeSeconds / combinedResult.getSuccessfulCustomerInteractions();

        if (tableMode) {
            System.out.println(threads + "," + successRatio + "," + customerRatio + "," + throughput + "," + latency);

        } else {
            System.out.println("Number of threads: " + threads);
            System.out.println("Success ratio: " + successRatio);
            System.out.println("Customer interaction ratio: " + customerRatio);
            System.out.println("Throughput: " + throughput + " successful customer interactions per second");
            System.out.println("Latency: " + latency + " seconds");
        }
    }

    /**
     * Generate the data in the inventory manager before the workload interactions are run.
     */
    private static void initializeInventoryManagerData(CustomerTransactionManager manager) throws InventoryManagerException {
        manager.processOrders(InventoryManagerMock.getInstance().generateValidPurchases(1000));
    }
}
