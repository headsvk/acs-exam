package com.acertaininventorymanager.workloads;

/**
 * WorkerRunResult class represents the result returned by a worker class after
 * running the workload interactions.
 */
class WorkerRunResult {

    /**
     * Total number of successful interactions.
     */
    private int mSuccessfulInteractions;

    /**
     * Total number of interactions run.
     */
    private int mTotalRuns;

    /**
     * Total time taken to run all interactions.
     */
    private long mElapsedTimeInNanoSecs;

    /**
     * Number of successful customer item purchase interactions.
     */
    private int mSuccessfulCustomerInteractions;

    /**
     * Total number of customer item purchase interactions.
     */
    private int mTotalCustomerInteractions;

    WorkerRunResult(int successfulInteractions, long elapsedTimeInNanoSecs,
                    int totalRuns, int mSuccessfulClientInteractions,
                    int mTotalCustomerInteractions) {
        this.setSuccessfulInteractions(successfulInteractions);
        this.setElapsedTimeInNanoSecs(elapsedTimeInNanoSecs);
        this.setTotalRuns(totalRuns);
        this.setSuccessfulCustomerInteractions(mSuccessfulClientInteractions);
        this.setTotalCustomerInteractions(mTotalCustomerInteractions);
    }

    int getTotalRuns() {
        return mTotalRuns;
    }

    void setTotalRuns(int totalRuns) {
        mTotalRuns = totalRuns;
    }

    int getSuccessfulInteractions() {
        return mSuccessfulInteractions;
    }

    void setSuccessfulInteractions(int successfulInteractions) {
        mSuccessfulInteractions = successfulInteractions;
    }

    long getElapsedTimeInNanoSecs() {
        return mElapsedTimeInNanoSecs;
    }

    void setElapsedTimeInNanoSecs(long elapsedTimeInNanoSecs) {
        mElapsedTimeInNanoSecs = elapsedTimeInNanoSecs;
    }

    int getSuccessfulCustomerInteractions() {
        return mSuccessfulCustomerInteractions;
    }

    void setSuccessfulCustomerInteractions(int successfulCustomerInteractions) {
        mSuccessfulCustomerInteractions = successfulCustomerInteractions;
    }

    int getTotalCustomerInteractions() {
        return mTotalCustomerInteractions;
    }

    void setTotalCustomerInteractions(int totalCustomerInteractions) {
        mTotalCustomerInteractions = totalCustomerInteractions;
    }

}
