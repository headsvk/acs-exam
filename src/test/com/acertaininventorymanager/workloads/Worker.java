package com.acertaininventorymanager.workloads;

import com.acertaininventorymanager.business.ItemPurchase;
import com.acertaininventorymanager.exceptions.InventoryManagerException;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Worker represents the workload runner which runs the workloads with
 * parameters using WorkloadConfiguration and then reports the results
 */
class Worker implements Callable<WorkerRunResult> {

    private WorkloadConfiguration mConfiguration = null;
    private int mSuccessfulCustomerInteractions = 0;
    private int mTotalCustomerInteractions = 0;

    Worker(WorkloadConfiguration config) {
        mConfiguration = config;
    }

    /**
     * Run the appropriate interaction while trying to maintain the configured
     * distribution.
     * Updates the counts of total runs and successful runs for customer
     * interactions.
     *
     * @param chooseInteraction Percentage from 0 to 100.
     * @return True if successful, false otherwise.
     */
    private boolean runInteraction(float chooseInteraction) {
        try {
            if (chooseInteraction < mConfiguration.getPercentRegionManagerInteraction()) {
                runRegionManagerInteraction();
            } else {
                mTotalCustomerInteractions++;
                runCustomerInteraction();
                mSuccessfulCustomerInteractions++;
            }
        } catch (InventoryManagerException ex) {
            return false;
        }
        return true;
    }

    /**
     * Run the workloads trying to respect the distributions of the interactions
     * and return result in the end
     */
    public WorkerRunResult call() throws Exception {
        int count = 1;
        int successfulInteractions = 0;
        long timeForRunsInNanoSecs = 0;

        Random rand = new Random();
        float chooseInteraction;

        // Perform the warm-up runs
        while (count++ <= mConfiguration.getWarmUpRuns()) {
            chooseInteraction = rand.nextFloat() * 100f;
            runInteraction(chooseInteraction);
        }

        count = 1;
        mTotalCustomerInteractions = 0;
        mSuccessfulCustomerInteractions = 0;

        // Perform the actual runs
        long startTimeInNanoSecs = System.nanoTime();
        while (count++ <= mConfiguration.getActualRuns()) {
            chooseInteraction = rand.nextFloat() * 100f;
            if (runInteraction(chooseInteraction)) {
                successfulInteractions++;
            }
        }
        long endTimeInNanoSecs = System.nanoTime();
        timeForRunsInNanoSecs += (endTimeInNanoSecs - startTimeInNanoSecs);
        return new WorkerRunResult(successfulInteractions, timeForRunsInNanoSecs, mConfiguration.getActualRuns(),
                mSuccessfulCustomerInteractions, mTotalCustomerInteractions);
    }

    /**
     * Runs the get totals for region interaction.
     *
     * @throws InventoryManagerException
     */
    private void runRegionManagerInteraction() throws InventoryManagerException {

        Set<Integer> regionIds = mConfiguration.getMock().getRandomRegions();

        mConfiguration.getTransactionManager().getTotalsForRegions(regionIds);
    }

    /**
     * Runs the process orders interaction.
     *
     * @throws InventoryManagerException
     */
    private void runCustomerInteraction() throws InventoryManagerException {

        Set<ItemPurchase> purchases = mConfiguration.getMock()
                .generateValidPurchases(mConfiguration.getItemPurchasesToAdd());

        mConfiguration.getTransactionManager().processOrders(purchases);
    }

}
