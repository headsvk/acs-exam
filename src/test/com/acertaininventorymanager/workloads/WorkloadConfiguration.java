package com.acertaininventorymanager.workloads;

import com.acertaininventorymanager.interfaces.CustomerTransactionManager;
import com.acertaininventorymanager.mock.InventoryManagerMock;

/**
 * WorkloadConfiguration represents the configuration parameters to be used by
 * Workers class for running the workloads.
 */
public class WorkloadConfiguration {

    private int mWarmUpRuns = 100;
    private int mActualRuns = 500;
    private int mItemPurchasesToAdd = 200;
    private float mPercentRegionManagerInteraction = 30f;

    private InventoryManagerMock mMock = InventoryManagerMock.getInstance();

    private CustomerTransactionManager mTransactionManager;

    public WorkloadConfiguration(CustomerTransactionManager transactionManager) {
        mTransactionManager = transactionManager;
    }

    public InventoryManagerMock getMock() {
        return mMock;
    }

    public CustomerTransactionManager getTransactionManager() {
        return mTransactionManager;
    }

    public int getWarmUpRuns() {
        return mWarmUpRuns;
    }

    public int getActualRuns() {
        return mActualRuns;
    }

    public float getPercentRegionManagerInteraction() {
        return mPercentRegionManagerInteraction;
    }

    public int getItemPurchasesToAdd() {
        return mItemPurchasesToAdd;
    }
}
