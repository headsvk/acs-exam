package com.acertaininventorymanager.client;

import com.acertaininventorymanager.business.CertainTransactionManager;
import com.acertaininventorymanager.business.ItemPurchase;
import com.acertaininventorymanager.business.RegionTotal;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.CustomerTransactionManager;
import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.acertaininventorymanager.utils.*;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.util.List;
import java.util.Set;

/**
 * {@link TransactionManagerHTTPProxy} implements the client level synchronous
 * {@link CertainTransactionManager} API declared in the {@link CustomerTransactionManager} class.
 *
 * @see CustomerTransactionManager
 * @see CertainTransactionManager
 */
public class TransactionManagerHTTPProxy implements CustomerTransactionManager {

    /**
     * The client.
     */
    private HttpClient client;

    /**
     * The server address.
     */
    private String serverAddress;

    /**
     * The serializer.
     */
    private static ThreadLocal<InventoryManagerSerializer> serializer;

    /**
     * Initializes a new {@link TransactionManagerHTTPProxy}.
     *
     * @param serverAddress the server address
     * @throws Exception the exception
     */
    public TransactionManagerHTTPProxy(String serverAddress) throws Exception {

        // Setup the type of serializer.
        if (InventoryManagerConstants.BINARY_SERIALIZATION) {
            serializer = ThreadLocal.withInitial(InventoryManagerKryoSerializer::new);
        } else {
            serializer = ThreadLocal.withInitial(InventoryManagerXStreamSerializer::new);
        }

        setServerAddress(serverAddress);
        client = new HttpClient();

        // Max concurrent connections to every address.
        client.setMaxConnectionsPerDestination(InventoryManagerClientConstants.CLIENT_MAX_CONNECTION_ADDRESS);

        // Max number of threads.
        client.setExecutor(new QueuedThreadPool(InventoryManagerClientConstants.CLIENT_MAX_THREADSPOOL_THREADS));

        // Seconds timeout; if no server reply, the request expires.
        client.setConnectTimeout(InventoryManagerClientConstants.CLIENT_MAX_TIMEOUT_MILLISECS);

        client.start();
    }

    /**
     * Gets the server address.
     *
     * @return the server address
     */
    public String getServerAddress() {
        return serverAddress;
    }

    /**
     * Sets the server address.
     *
     * @param serverAddress the new server address
     */
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    /**
     * Stops the proxy.
     */
    public void stop() {
        try {
            client.stop();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void processOrders(Set<ItemPurchase> itemPurchases) throws InventoryManagerException {
        String urlString = serverAddress + "/" + InventoryManagerMessageTag.PROCESS_ORDERS;
        InventoryManagerRequest bookStoreRequest = InventoryManagerRequest.newPostRequest(urlString, itemPurchases);
        InventoryManagerUtility.performHttpExchange(client, bookStoreRequest, serializer.get());
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<RegionTotal> getTotalsForRegions(Set<Integer> regionIds) throws InventoryManagerException {
        String urlString = serverAddress + "/" + InventoryManagerMessageTag.REGION_TOTALS;
        InventoryManagerRequest bookStoreRequest = InventoryManagerRequest.newPostRequest(urlString, regionIds);
        InventoryManagerResponse bookStoreResponse = InventoryManagerUtility.performHttpExchange(client,
                bookStoreRequest, serializer.get());
        return (List<RegionTotal>) bookStoreResponse.getList();
    }

    public void clearValuesBought() throws InventoryManagerException {
        String urlString = serverAddress + "/" + InventoryManagerMessageTag.CLEAR_VALUES_BOUGHT;
        InventoryManagerRequest bookStoreRequest = InventoryManagerRequest.newPostRequest(urlString, "");
        InventoryManagerUtility.performHttpExchange(client, bookStoreRequest, serializer.get());
    }
}
