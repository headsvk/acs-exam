package com.acertaininventorymanager.client;

import com.acertaininventorymanager.business.CertainDataManager;
import com.acertaininventorymanager.business.CertainTransactionManager;
import com.acertaininventorymanager.business.ItemPurchase;
import com.acertaininventorymanager.business.RegionTotal;
import com.acertaininventorymanager.exceptions.InexistentItemPurchaseException;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.CustomerTransactionManager;
import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.acertaininventorymanager.interfaces.ItemDataManager;
import com.acertaininventorymanager.utils.*;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Set;

/**
 * {@link DataManagerHTTPProxy} implements the client level synchronous
 * {@link ItemDataManager} API declared in the {@link CertainDataManager} class.
 *
 * @see ItemDataManager
 * @see CertainDataManager
 */
public class DataManagerHTTPProxy implements ItemDataManager {

    /**
     * The client.
     */
    private HttpClient client;

    /**
     * The server address.
     */
    private String serverAddress;

    /**
     * The serializer.
     */
    private static ThreadLocal<InventoryManagerSerializer> serializer;

    /**
     * Initializes a new {@link DataManagerHTTPProxy}.
     *
     * @param serverAddress the server address
     * @throws Exception the exception
     */
    public DataManagerHTTPProxy(String serverAddress) throws Exception {

        // Setup the type of serializer.
        if (InventoryManagerConstants.BINARY_SERIALIZATION) {
            serializer = ThreadLocal.withInitial(InventoryManagerKryoSerializer::new);
        } else {
            serializer = ThreadLocal.withInitial(InventoryManagerXStreamSerializer::new);
        }

        setServerAddress(serverAddress);
        client = new HttpClient();

        // Max concurrent connections to every address.
        client.setMaxConnectionsPerDestination(InventoryManagerClientConstants.CLIENT_MAX_CONNECTION_ADDRESS);

        // Max number of threads.
        client.setExecutor(new QueuedThreadPool(InventoryManagerClientConstants.CLIENT_MAX_THREADSPOOL_THREADS));

        // Seconds timeout; if no server reply, the request expires.
        client.setConnectTimeout(InventoryManagerClientConstants.CLIENT_MAX_TIMEOUT_MILLISECS);

        client.start();
    }

    /**
     * Gets the server address.
     *
     * @return the server address
     */
    public String getServerAddress() {
        return serverAddress;
    }

    /**
     * Sets the server address.
     *
     * @param serverAddress the new server address
     */
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    /**
     * Stops the proxy.
     */
    public void stop() {
        try {
            client.stop();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void addItemPurchase(ItemPurchase itemPurchase) throws InventoryManagerException {
        String urlString = serverAddress + "/" + InventoryManagerMessageTag.ADD_ITEM_PURCHASE;
        InventoryManagerRequest bookStoreRequest = InventoryManagerRequest.newPostRequest(urlString, itemPurchase);
        InventoryManagerUtility.performHttpExchange(client, bookStoreRequest, serializer.get());
    }

    @Override
    public void removeItemPurchase(int orderId, int customerId, int itemId) throws InventoryManagerException {
        String orderIdString, customerIdString, itemIdString;

        try {
            orderIdString = URLEncoder.encode(Integer.toString(orderId), "UTF-8");
            customerIdString = URLEncoder.encode(Integer.toString(customerId), "UTF-8");
            itemIdString = URLEncoder.encode(Integer.toString(itemId), "UTF-8");

        } catch (UnsupportedEncodingException ex) {
            throw new InventoryManagerException("Unsupported encoding of arguments", ex);
        }

        String urlString = serverAddress + "/" + InventoryManagerMessageTag.REMOVE_ITEM_PURCHASE + "?" +
                InventoryManagerConstants.ORDER_ID_PARAM + "=" + orderIdString + "&" +
                InventoryManagerConstants.CUSTOMER_ID_PARAM + "=" + customerIdString + "&" +
                InventoryManagerConstants.ITEM_ID_PARAM + "=" + itemIdString;

        InventoryManagerRequest bookStoreRequest = InventoryManagerRequest.newPostRequest(urlString, "");
        InventoryManagerUtility.performHttpExchange(client, bookStoreRequest, serializer.get());
    }
}
