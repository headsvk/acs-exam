package com.acertaininventorymanager.utils;

import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.io.*;

/**
 * {@link InventoryManagerKryoSerializer} serializes objects to arrays of bytes
 * representing strings using the Kryo library.
 * 
 * @see InventoryManagerSerializer
 */
public final class InventoryManagerKryoSerializer implements InventoryManagerSerializer {

	/** The binary stream. */
	private final Kryo binaryStream;

	/**
	 * Instantiates a new {@link InventoryManagerKryoSerializer}.
	 */
	public InventoryManagerKryoSerializer() {
		binaryStream = new Kryo();
		binaryStream.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
	}

	@Override
	public byte[] serialize(Object object) throws IOException {
		try (ByteArrayOutputStream outStream = new ByteArrayOutputStream(); Output out = new Output(outStream)) {
			binaryStream.writeClassAndObject(out, object);
			out.flush();
			return outStream.toByteArray();
		}
	}

	@Override
	public Object deserialize(byte[] bytes) throws IOException {
		try (InputStream inStream = new ByteArrayInputStream(bytes); Input in = new Input(inStream)) {
			return binaryStream.readClassAndObject(in);
		}
	}
}
