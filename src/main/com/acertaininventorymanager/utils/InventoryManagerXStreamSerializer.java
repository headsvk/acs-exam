package com.acertaininventorymanager.utils;

import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * {@link InventoryManagerXStreamSerializer} serializes objects to arrays of bytes
 * representing XML trees using the XStream library.
 * 
 * @see InventoryManagerSerializer
 */
public final class InventoryManagerXStreamSerializer implements InventoryManagerSerializer {

	/** The XML stream. */
	private final XStream xmlStream = new XStream(new StaxDriver());

	@Override
	public byte[] serialize(Object object) {
		String xml = xmlStream.toXML(object);
		return xml.getBytes();
	}

	@Override
	public Object deserialize(byte[] bytes) {
		String xml = new String(bytes);
		return xmlStream.fromXML(xml);
	}
}
