package com.acertaininventorymanager.utils;

/**
 * {@link InventoryManagerMessageTag} implements the messages supported in the inventory manager.
 */
public enum InventoryManagerMessageTag {

	/** The tag for the process orders message. */
	PROCESS_ORDERS,

    /** The tag for the region totals message. */
    REGION_TOTALS,

    /** The tag for the add item purchase message. */
    ADD_ITEM_PURCHASE,

    /** The tag for the remove item purchase message. */
    REMOVE_ITEM_PURCHASE,

    /** The tag for the testing clear values bought message. */
    CLEAR_VALUES_BOUGHT,

}
