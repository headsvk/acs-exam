package com.acertaininventorymanager.utils;

import com.acertaininventorymanager.client.InventoryManagerClientConstants;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentProvider;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.util.BytesContentProvider;
import org.eclipse.jetty.http.HttpMethod;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * {@link InventoryManagerUtility} implements utility methods used by the bookstore
 * server and client.
 */
public final class InventoryManagerUtility {

    /**
     * Prevents the instantiation of a new {@link InventoryManagerUtility}.
     */
    private InventoryManagerUtility() {
        // Prevent instantiation.
    }


    /**
     * Converts a string to an integer if possible else it returns the signal
     * value for failure passed as parameter.
     *
     * @param str the string
     * @return the integer
     * @throws InventoryManagerException the book store exception
     */
    public static int convertStringToInt(String str) throws InventoryManagerException {

        try {
            return Integer.parseInt(str);
        } catch (Exception ex) {
            throw new InventoryManagerException(ex);
        }
    }

    /**
     * Convert a request URI to the message tags supported in CertainBookStore.
     *
     * @param requestURI the request URI
     * @return the book store message tag
     */
    public static InventoryManagerMessageTag convertURItoMessageTag(String requestURI) {

        try {
            return InventoryManagerMessageTag.valueOf(requestURI.substring(1).toUpperCase());
        } catch (IllegalArgumentException | NullPointerException ex) {
            // Enumeration type matching failed so non supported message or the
            // request URI was empty.
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * Perform HTTP exchange.
     *
     * @param client                  the client
     * @param inventoryManagerRequest the book store request
     * @param serializer              the serializer
     * @return the book store response
     * @throws InventoryManagerException the book store exception
     */
    public static InventoryManagerResponse performHttpExchange(HttpClient client, InventoryManagerRequest inventoryManagerRequest,
                                                               InventoryManagerSerializer serializer) throws InventoryManagerException {
        Request request;

        switch (inventoryManagerRequest.getMethod()) {
            case GET:
                request = client.newRequest(inventoryManagerRequest.getURLString()).method(HttpMethod.GET);
                break;

            case POST:
                try {
                    byte[] serializedValue = serializer.serialize(inventoryManagerRequest.getInputValue());
                    ContentProvider contentProvider = new BytesContentProvider(serializedValue);
                    request = client.POST(inventoryManagerRequest.getURLString()).content(contentProvider);
                } catch (IOException ex) {
                    throw new InventoryManagerException("Serialization error", ex);
                }

                break;

            default:
                throw new IllegalArgumentException("HTTP Method not supported.");
        }

        ContentResponse response;

        try {
            response = request.send();
        } catch (InterruptedException ex) {
            throw new InventoryManagerException(InventoryManagerClientConstants.STR_ERR_CLIENT_REQUEST_SENDING, ex);
        } catch (TimeoutException ex) {
            throw new InventoryManagerException(InventoryManagerClientConstants.STR_ERR_CLIENT_REQUEST_TIMEOUT, ex);
        } catch (ExecutionException ex) {
            throw new InventoryManagerException(InventoryManagerClientConstants.STR_ERR_CLIENT_REQUEST_EXCEPTION, ex);
        }

        InventoryManagerResponse inventoryManagerResponse;

        try {
            inventoryManagerResponse = (InventoryManagerResponse) serializer.deserialize(response.getContent());
        } catch (Exception ex) {
            throw new InventoryManagerException("Deserialization error", ex);
        }

        InventoryManagerException exception = inventoryManagerResponse.getException();

        if (exception != null) {
            throw exception;
        }

        return inventoryManagerResponse;
    }
}
