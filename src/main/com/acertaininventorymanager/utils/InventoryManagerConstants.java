package com.acertaininventorymanager.utils;

/**
 * InventoryManagerConstants declares the constants used in the CertainBookStore (by
 * both servers and clients).
 */
public final class InventoryManagerConstants {

	/**
	 * The Constant BINARY_SERIALIZATION decides whether we use Kryo or XStream.
	 */
	public static final boolean BINARY_SERIALIZATION = true;

    // Constants used when creating URLs

    /** The Constant ORDER_ID_PARAM. */
    public static final String ORDER_ID_PARAM = "orderId";

    /** The Constant CUSTOMER_ID_PARAM. */
    public static final String CUSTOMER_ID_PARAM = "customerId";

    /** The Constant ITEM_ID_PARAM. */
    public static final String ITEM_ID_PARAM = "itemId";

	/** The Constant PROPERTY_KEY_LOCAL_TEST. */
	public static final String PROPERTY_KEY_LOCAL_TEST = "localTest";

	/** The Constant PROPERTY_KEY_SERVER_PORT. */
	public static final String PROPERTY_KEY_SERVER_PORT = "port";

	/**
	 * Prevents the instantiation of a new {@link InventoryManagerConstants}.
	 */
	private InventoryManagerConstants() {
		// Prevent instantiation.
	}
}
