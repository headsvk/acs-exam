package com.acertaininventorymanager.utils;

import com.acertaininventorymanager.exceptions.InventoryManagerException;

import java.util.List;

/**
 * {@link InventoryManagerResponse} is the data structure that encapsulates a HTTP
 * response from the bookstore server to the client. The data structure contains
 * error messages from the server if an error occurred.
 */
public class InventoryManagerResponse {

	/** The exception. */
	private InventoryManagerException exception;

	/** The list. */
	private List<?> list;

	/**
	 * Instantiates a new {@link InventoryManagerResponse}.
	 *
	 * @param exception
	 *            the exception
	 * @param list
	 *            the list
	 */
	public InventoryManagerResponse(InventoryManagerException exception, List<?> list) {
		this.setException(exception);
		this.setList(list);
	}

	/**
	 * Instantiates a new book store response.
	 */
	public InventoryManagerResponse() {
		this.setException(null);
		this.setList(null);
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<?> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list
	 *            the new list
	 */
	public void setList(List<?> list) {
		this.list = list;
	}

	/**
	 * Gets the exception.
	 *
	 * @return the exception
	 */
	public InventoryManagerException getException() {
		return exception;
	}

	/**
	 * Sets the exception.
	 *
	 * @param exception
	 *            the new exception
	 */
	public void setException(InventoryManagerException exception) {
		this.exception = exception;
	}
}
