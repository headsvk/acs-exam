package com.acertaininventorymanager.server;

import com.acertaininventorymanager.business.CertainTransactionManager;
import com.acertaininventorymanager.client.DataManagerHTTPProxy;
import com.acertaininventorymanager.interfaces.ItemDataManager;
import com.acertaininventorymanager.mock.InventoryManagerMock;
import com.acertaininventorymanager.utils.InventoryManagerConstants;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Starts the {@link TransactionManagerHTTPServer} that the clients will communicate with.
 */
public class TransactionManagerHTTPServer {

    /**
     * The Constant defaultListenOnPort.
     */
    private static final int DEFAULT_PORT = 8081;
    private static final int MIN_THREADPOOL_SIZE = 10;
    private static final int MAX_THREADPOOL_SIZE = 100;

    private static final String SERVER_PROPERTIES_PATH = "./server.properties";
    private static final String SERVER_PROPERTIES_KEY = "itemDataManagers";

    /**
     * Prevents the instantiation of a new {@link TransactionManagerHTTPServer}.
     */
    private TransactionManagerHTTPServer() {
        // Prevent instances from being created.
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        CertainTransactionManager transactionManager = null;
        try {
            transactionManager = new CertainTransactionManager(initializeItemDataManagers(),
                    InventoryManagerMock.getInstance().getCustomers());

        } catch (Exception e) {
            System.err.println("Failed to initialize data manager proxies.");
            e.printStackTrace();
            System.exit(101);
        }

        int listenOnPort = DEFAULT_PORT;

        TransactionManagerHTTPMessageHandler handler = new TransactionManagerHTTPMessageHandler(transactionManager);
        String serverPortString = System.getProperty(InventoryManagerConstants.PROPERTY_KEY_SERVER_PORT);

        if (serverPortString != null) {
            try {
                listenOnPort = Integer.parseInt(serverPortString);
            } catch (NumberFormatException ex) {
                System.err.println("Unsupported message tag");
            }
        }

        QueuedThreadPool threadpool = new QueuedThreadPool(MAX_THREADPOOL_SIZE, MIN_THREADPOOL_SIZE);
        InventoryManagerHTTPServerUtility.createServer(listenOnPort, handler, threadpool);
    }


    /**
     * Initializes a list of {@link DataManagerHTTPProxy} from the server properties file.
     */
    private static List<ItemDataManager> initializeItemDataManagers() throws Exception {
        Properties props = new Properties();
        props.load(new FileInputStream(SERVER_PROPERTIES_PATH));

        String addresses = props.getProperty(SERVER_PROPERTIES_KEY);

        List<ItemDataManager> dataManagers = new ArrayList<>();

        for (String address : addresses.split(";")) {
            if (!address.toLowerCase().startsWith("http://")) {
                address = "http://" + address;
            }

            dataManagers.add(new DataManagerHTTPProxy(address));
        }
        return dataManagers;
    }
}
