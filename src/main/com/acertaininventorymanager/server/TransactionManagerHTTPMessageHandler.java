package com.acertaininventorymanager.server;

import com.acertaininventorymanager.business.CertainTransactionManager;
import com.acertaininventorymanager.business.ItemPurchase;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.acertaininventorymanager.utils.*;
import com.esotericsoftware.kryo.io.Input;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * {@link TransactionManagerHTTPMessageHandler} implements the message handler class
 * which is invoked to handle messages received by the
 * {@link InventoryManagerHTTPServerUtility}. It decodes the HTTP message and invokes
 * the {@link CertainTransactionManager} server API.
 *
 * @see AbstractHandler
 * @see InventoryManagerHTTPServerUtility
 * @see CertainTransactionManager
 */
public class TransactionManagerHTTPMessageHandler extends AbstractHandler {

    /**
     * The customer transaction manager.
     */
    private CertainTransactionManager mTransactionManager = null;

    /**
     * The sSerializer.
     */
    private static ThreadLocal<InventoryManagerSerializer> sSerializer;

    /**
     * Instantiates a new {@link TransactionManagerHTTPMessageHandler}.
     *
     * @param transactionManager the transaction manager
     */
    public TransactionManagerHTTPMessageHandler(CertainTransactionManager transactionManager) {
        mTransactionManager = transactionManager;

        // Setup the type of sSerializer.
        if (InventoryManagerConstants.BINARY_SERIALIZATION) {
            sSerializer = ThreadLocal.withInitial(InventoryManagerKryoSerializer::new);
        } else {
            sSerializer = ThreadLocal.withInitial(InventoryManagerXStreamSerializer::new);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.jetty.server.Handler#handle(java.lang.String,
     * org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        InventoryManagerMessageTag messageTag;
        String requestURI;

        response.setStatus(HttpServletResponse.SC_OK);
        requestURI = request.getRequestURI();
        messageTag = InventoryManagerUtility.convertURItoMessageTag(requestURI);

        // The RequestURI before the switch.
        if (messageTag == null) {
            System.err.println("No message tag.");
        } else {
            switch (messageTag) {
                case PROCESS_ORDERS:
                    processOrders(request, response);
                    break;

                case REGION_TOTALS:
                    getTotalsForRegions(request, response);
                    break;

                case CLEAR_VALUES_BOUGHT:
                    clearValuesBought(response);
                    break;

                default:
                    System.err.println("Unsupported message tag.");
                    break;
            }
        }

        // Mark the request as handled so that the HTTP response can be sent
        baseRequest.setHandled(true);
    }

    /**
     * Gets totals for region.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    private void getTotalsForRegions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        byte[] serializedRequestContent = getSerializedRequestContent(request);

        Set<Integer> regionIds = (Set<Integer>) sSerializer.get().deserialize(serializedRequestContent);
        InventoryManagerResponse inventoryManagerResponse = new InventoryManagerResponse();

        try {
            inventoryManagerResponse.setList(mTransactionManager.getTotalsForRegions(regionIds));
        } catch (InventoryManagerException ex) {
            inventoryManagerResponse.setException(ex);
        }

        byte[] serializedResponseContent = sSerializer.get().serialize(inventoryManagerResponse);
        response.getOutputStream().write(serializedResponseContent);
    }

    /**
     * Processes orders.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    private void processOrders(HttpServletRequest request, HttpServletResponse response) throws IOException {
        byte[] serializedRequestContent = getSerializedRequestContent(request);

        Set<ItemPurchase> itemPurchases = (Set<ItemPurchase>) sSerializer.get().deserialize(serializedRequestContent);
        InventoryManagerResponse inventoryManagerResponse = new InventoryManagerResponse();

        try {
            mTransactionManager.processOrders(itemPurchases);
        } catch (InventoryManagerException ex) {
            inventoryManagerResponse.setException(ex);
        }

        byte[] serializedResponseContent = sSerializer.get().serialize(inventoryManagerResponse);
        response.getOutputStream().write(serializedResponseContent);
    }

    /**
     * Removes all orders.
     *
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void clearValuesBought(HttpServletResponse response) throws IOException {

        InventoryManagerResponse inventoryManagerResponse = new InventoryManagerResponse();
        mTransactionManager.clearValuesBought();

        byte[] serializedResponseContent = sSerializer.get().serialize(inventoryManagerResponse);
        response.getOutputStream().write(serializedResponseContent);
    }

    /**
     * Gets the serialized request content.
     *
     * @param request the request
     * @return the serialized request content
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private byte[] getSerializedRequestContent(HttpServletRequest request) throws IOException {
        Input in = new Input(request.getInputStream());
        byte[] serializedRequestContent = in.readBytes(request.getContentLength());
        in.close();
        return serializedRequestContent;
    }
}
