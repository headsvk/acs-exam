package com.acertaininventorymanager.server;

import com.acertaininventorymanager.business.CertainDataManager;
import com.acertaininventorymanager.business.ItemPurchase;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.InventoryManagerSerializer;
import com.acertaininventorymanager.utils.*;
import com.esotericsoftware.kryo.io.Input;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Set;

/**
 * {@link DataManagerHTTPMessageHandler} implements the message handler class
 * which is invoked to handle messages received by the
 * {@link InventoryManagerHTTPServerUtility}. It decodes the HTTP message and invokes
 * the {@link CertainDataManager} server API.
 *
 * @see AbstractHandler
 * @see InventoryManagerHTTPServerUtility
 * @see CertainDataManager
 */
public class DataManagerHTTPMessageHandler extends AbstractHandler {

    /**
     * The item data manager.
     */
    private CertainDataManager mDataManager = null;

    /**
     * The sSerializer.
     */
    private static ThreadLocal<InventoryManagerSerializer> sSerializer;

    /**
     * Instantiates a new {@link DataManagerHTTPMessageHandler}.
     *
     * @param dataManager the transaction manager
     */
    public DataManagerHTTPMessageHandler(CertainDataManager dataManager) {
        mDataManager = dataManager;

        // Setup the type of sSerializer.
        if (InventoryManagerConstants.BINARY_SERIALIZATION) {
            sSerializer = ThreadLocal.withInitial(InventoryManagerKryoSerializer::new);
        } else {
            sSerializer = ThreadLocal.withInitial(InventoryManagerXStreamSerializer::new);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.jetty.server.Handler#handle(java.lang.String,
     * org.eclipse.jetty.server.Request, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        InventoryManagerMessageTag messageTag;
        String requestURI;

        response.setStatus(HttpServletResponse.SC_OK);
        requestURI = request.getRequestURI();
        messageTag = InventoryManagerUtility.convertURItoMessageTag(requestURI);

        // The RequestURI before the switch.
        if (messageTag == null) {
            System.err.println("No message tag.");
        } else {
            switch (messageTag) {
                case ADD_ITEM_PURCHASE:
                    addItemPurchase(request, response);
                    break;

                case REMOVE_ITEM_PURCHASE:
                    removeItemPurchase(request, response);
                    break;

                default:
                    System.err.println("Unsupported message tag.");
                    break;
            }
        }

        // Mark the request as handled so that the HTTP response can be sent
        baseRequest.setHandled(true);
    }

    /**
     * Removes an item purchase.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    private void removeItemPurchase(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String orderIdString = URLDecoder.decode(request.getParameter(InventoryManagerConstants.ORDER_ID_PARAM), "UTF-8");
        String customerIdString = URLDecoder.decode(request.getParameter(InventoryManagerConstants.CUSTOMER_ID_PARAM), "UTF-8");
        String itemIdString = URLDecoder.decode(request.getParameter(InventoryManagerConstants.ITEM_ID_PARAM), "UTF-8");

        InventoryManagerResponse inventoryManagerResponse = new InventoryManagerResponse();

        try {
            int orderId = InventoryManagerUtility.convertStringToInt(orderIdString);
            int customerId = InventoryManagerUtility.convertStringToInt(customerIdString);
            int itemId = InventoryManagerUtility.convertStringToInt(itemIdString);
            mDataManager.removeItemPurchase(orderId, customerId, itemId);

        } catch (InventoryManagerException ex) {
            inventoryManagerResponse.setException(ex);
        }

        byte[] serializedResponseContent = sSerializer.get().serialize(inventoryManagerResponse);
        response.getOutputStream().write(serializedResponseContent);
    }

    /**
     * Adds an item purchase.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    private void addItemPurchase(HttpServletRequest request, HttpServletResponse response) throws IOException {
        byte[] serializedRequestContent = getSerializedRequestContent(request);

        ItemPurchase itemPurchase = (ItemPurchase) sSerializer.get().deserialize(serializedRequestContent);
        InventoryManagerResponse inventoryManagerResponse = new InventoryManagerResponse();

        try {
            mDataManager.addItemPurchase(itemPurchase);
        } catch (InventoryManagerException ex) {
            inventoryManagerResponse.setException(ex);
        }

        byte[] serializedResponseContent = sSerializer.get().serialize(inventoryManagerResponse);
        response.getOutputStream().write(serializedResponseContent);
    }

    /**
     * Gets the serialized request content.
     *
     * @param request the request
     * @return the serialized request content
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private byte[] getSerializedRequestContent(HttpServletRequest request) throws IOException {
        Input in = new Input(request.getInputStream());
        byte[] serializedRequestContent = in.readBytes(request.getContentLength());
        in.close();
        return serializedRequestContent;
    }
}
