package com.acertaininventorymanager.server;

import com.acertaininventorymanager.business.CertainDataManager;
import com.acertaininventorymanager.utils.InventoryManagerConstants;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

/**
 * Starts the {@link DataManagerHTTPServer} that the clients will communicate with.
 */
public class DataManagerHTTPServer {

    /**
     * The Constant defaultListenOnPort.
     */
    private static final int DEFAULT_PORT = 8082;
    private static final int MIN_THREADPOOL_SIZE = 10;
    private static final int MAX_THREADPOOL_SIZE = 100;

    /**
     * Prevents the instantiation of a new {@link DataManagerHTTPServer}.
     */
    private DataManagerHTTPServer() {
        // Prevent instances from being created.
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        CertainDataManager dataManager = new CertainDataManager();

        int listenOnPort = DEFAULT_PORT;

        DataManagerHTTPMessageHandler handler = new DataManagerHTTPMessageHandler(dataManager);
        String serverPortString = System.getProperty(InventoryManagerConstants.PROPERTY_KEY_SERVER_PORT);

        if (serverPortString != null) {
            try {
                listenOnPort = Integer.parseInt(serverPortString);
            } catch (NumberFormatException ex) {
                System.err.println("Unsupported message tag");
            }
        }

        QueuedThreadPool threadpool = new QueuedThreadPool(MAX_THREADPOOL_SIZE, MIN_THREADPOOL_SIZE);
        InventoryManagerHTTPServerUtility.createServer(listenOnPort, handler, threadpool);
    }
}
