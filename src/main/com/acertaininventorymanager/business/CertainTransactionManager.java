package com.acertaininventorymanager.business;

import com.acertaininventorymanager.exceptions.EmptyRegionException;
import com.acertaininventorymanager.exceptions.InexistentCustomerException;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.exceptions.NonPositiveIntegerException;
import com.acertaininventorymanager.interfaces.CustomerTransactionManager;
import com.acertaininventorymanager.interfaces.ItemDataManager;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class CertainTransactionManager implements CustomerTransactionManager {

    // Stores customers indexed by ids
    private final Map<Integer, Customer> mCustomers = new HashMap<>();

    // Stores customer ids per region for faster look up along with region locks
    private final Map<Integer, RegionLock> mRegions = new HashMap<>();

    private final List<ItemDataManager> mItemDataManagers;

    public CertainTransactionManager(List<ItemDataManager> itemDataManagers, Set<Customer> customers) {
        mItemDataManagers = itemDataManagers;

        // Process customers
        for (Customer customer : customers) {
            mCustomers.put(customer.getCustomerId(), customer); // add customer to customer map

            // add customer to region map
            RegionLock regionLock = mRegions.computeIfAbsent(customer.getRegionId(), id -> new RegionLock());
            regionLock.mCustomerIds.add(customer.getCustomerId());
        }
    }

    private ItemDataManager getDataManagerForPurchase(ItemPurchase purchase) {
        return mItemDataManagers.get(purchase.getItemId() % mItemDataManagers.size());
    }

    @Override
    public void processOrders(Set<ItemPurchase> itemPurchases) throws InventoryManagerException {

        // Check if item purchase properties are valid
        for (ItemPurchase itemPurchase : itemPurchases) {
            if (itemPurchase.getOrderId() <= 0) {
                throw new NonPositiveIntegerException(
                        "OrderId = " + itemPurchase.getOrderId() + " is not positive");
            }
            if (itemPurchase.getQuantity() <= 0) {
                throw new NonPositiveIntegerException(
                        "OrderId = " + itemPurchase.getOrderId() + " quantity is not positive");
            }
            if (itemPurchase.getUnitPrice() <= 0) {
                throw new NonPositiveIntegerException(
                        "OrderId = " + itemPurchase.getOrderId() + " unit price is not positive");
            }
            if (!mCustomers.containsKey(itemPurchase.getCustomerId())) {
                throw new InexistentCustomerException(
                        "CustomerId = " + itemPurchase.getCustomerId() + " does not exist");
            }
        }

        // Get locks for all regions sorted by id.
        List<Lock> regionLocks = itemPurchases.stream()
                .mapToInt(itemPurchase -> mCustomers.get(itemPurchase.getCustomerId()).getRegionId())
                .distinct()
                .sorted()
                .boxed()
                .map(regionId -> mRegions.get(regionId).mLock.writeLock())
                .collect(Collectors.toList());

        regionLocks.forEach(Lock::lock);  // lock all locks

        List<ItemPurchase> addedPurchases = new ArrayList<>();

        try {

            // Add purchases to their respective item data managers (might throw exceptions)
            for (ItemPurchase itemPurchase : itemPurchases) {
                ItemDataManager itemDataManager = getDataManagerForPurchase(itemPurchase);
                itemDataManager.addItemPurchase(itemPurchase);
                addedPurchases.add(itemPurchase);   // add successful purchase for rollback
            }

            // Update values bought by the customers after all purchases succeeded
            for (ItemPurchase itemPurchase : itemPurchases) {
                Customer customer = mCustomers.get(itemPurchase.getCustomerId());
                customer.addValueBought(itemPurchase.getTotalValue());
            }

        } catch (InventoryManagerException exception) {

            // Safely rollback all added purchases
            for (ItemPurchase addedPurchase : addedPurchases) {
                try {
                    // Try to rollback the purchase
                    ItemDataManager itemDataManager = getDataManagerForPurchase(addedPurchase);
                    itemDataManager.removeItemPurchase(addedPurchase.getOrderId(),
                            addedPurchase.getCustomerId(), addedPurchase.getItemId());

                } catch (InventoryManagerException ex) {
                    // Nothing else to do, just log the exception and proceed
                    System.err.println("Failed to rollback purchase, orderId = " + addedPurchase.getOrderId());
                    ex.printStackTrace();
                }
            }

            throw exception;    // rethrow the exception

        } finally {
            regionLocks.forEach(Lock::unlock);    // unlock all locks
        }
    }

    @Override
    public List<RegionTotal> getTotalsForRegions(Set<Integer> regionIds) throws InventoryManagerException {

        // Check if region ids are valid
        for (Integer regionId : regionIds) {
            if (regionId <= 0) {
                throw new NonPositiveIntegerException("RegionId = " + regionId + " is not positive");
            }
            RegionLock regionLock = mRegions.get(regionId);
            if (regionLock == null || regionLock.mCustomerIds.isEmpty()) {
                throw new EmptyRegionException("RegionId = " + regionId + " is empty");
            }
        }

        // Get locks for all regions sorted by id.
        List<Lock> regionLocks = regionIds.stream()
                .sorted()
                .map(regionId -> mRegions.get(regionId).mLock.readLock())
                .collect(Collectors.toList());

        regionLocks.forEach(Lock::lock);      // lock all locks

        List<RegionTotal> regionTotals = regionIds.stream()
                .map(this::getTotalForSingleRegion)
                .collect(Collectors.toList());

        regionLocks.forEach(Lock::unlock);    // unlock all locks

        return regionTotals;
    }

    /**
     * Resets values bought by the customers.
     */
    public void clearValuesBought() {
        mCustomers.values().forEach(customer -> customer.setValueBought(0));
    }

    /**
     * Helper method that returns totals for a single region.
     */
    private RegionTotal getTotalForSingleRegion(int regionId) {
        long totalValueBought = mRegions.get(regionId).mCustomerIds.stream()
                .mapToLong(customerId -> mCustomers.get(customerId).getValueBought())
                .sum();
        return new RegionTotal(regionId, totalValueBought);
    }

    /**
     * Helper class that customer ids and a lock for a region.
     */
    private static class RegionLock {
        Set<Integer> mCustomerIds = new HashSet<>();
        ReadWriteLock mLock = new ReentrantReadWriteLock();
    }
}
