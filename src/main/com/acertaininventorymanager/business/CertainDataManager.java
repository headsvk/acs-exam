package com.acertaininventorymanager.business;

import com.acertaininventorymanager.exceptions.InexistentItemPurchaseException;
import com.acertaininventorymanager.exceptions.InventoryManagerException;
import com.acertaininventorymanager.interfaces.ItemDataManager;

import java.util.HashSet;
import java.util.Set;

public class CertainDataManager implements ItemDataManager {

    private final Set<ItemPurchase> mItemPurchases = new HashSet<>();

    @Override
    public synchronized void addItemPurchase(ItemPurchase itemPurchase)
            throws InventoryManagerException {

        mItemPurchases.add(itemPurchase);   // simply add the purchase
    }

    @Override
    public synchronized void removeItemPurchase(int orderId, int customerId, int itemId)
            throws InventoryManagerException {

        // create a dummy purchase for comparison
        ItemPurchase purchase = new ItemPurchase(orderId, customerId, itemId, 0, 0);

        // remove the purchase
        if (!mItemPurchases.remove(purchase)) {
            throw new InexistentItemPurchaseException("Item purchase orderId = " + orderId + " does not exist");
        }
    }
}
