package com.acertaininventorymanager.mock;

import com.acertaininventorymanager.business.Customer;
import com.acertaininventorymanager.business.ItemPurchase;

import java.util.*;

public class InventoryManagerMock {

    private static final int CUSTOMERS_COUNT = 20000;

    private static final int REGIONS_COUNT = 1000;

    // setting seed guarantees the same customers will be generated for server and test
    private Random mRandom = new Random(1);

    private Set<Customer> mCustomers = new HashSet<>();
    private Map<Integer, List<Integer>> mRegionMap = new HashMap<>();

    private int mOrderId = 1;

    private static volatile InventoryManagerMock mMock;

    public static InventoryManagerMock getInstance() {
        return mMock != null ? mMock : (mMock = new InventoryManagerMock());
    }

    private InventoryManagerMock() {

        for (int id = 1; id <= REGIONS_COUNT; id++) {
            mRegionMap.put(id, new ArrayList<>());
        }

        for (int id = 0; id < CUSTOMERS_COUNT; id++) {
            int regionId = generateRegionId();
            mCustomers.add(new Customer(id, regionId));
            mRegionMap.get(regionId).add(id);
        }
    }

    public Set<Customer> getCustomers() {
        return mCustomers;
    }

    public int getCustomersCount() {
        return CUSTOMERS_COUNT;
    }

    public int getRegionsCount() {
        return REGIONS_COUNT;
    }

    public Set<Integer> getAllRegions() {
        Set<Integer> regionIds = new HashSet<>(REGIONS_COUNT);
        for (int i = 1; i <= REGIONS_COUNT; i++) {
            regionIds.add(i);
        }
        return regionIds;
    }

    public Set<Integer> getRandomRegions() {
        int regionsCount = mRandom.nextInt(REGIONS_COUNT / 10);
        Set<Integer> regionIds = new HashSet<>(regionsCount);
        while (regionIds.size() < regionsCount) {
            regionIds.add(mRandom.nextInt(REGIONS_COUNT) + 1);
        }
        return regionIds;
    }

    public Set<ItemPurchase> generateValidPurchases(int count) {
        // only include customers from one region!
        List<Integer> customerIds = mRegionMap.get(generateRegionId());
        Set<ItemPurchase> purchases = new HashSet<>(count);

        for (int i = 0; i < count; i++) {
            // pick a random customer from the region
            int customerId = customerIds.get(mRandom.nextInt(customerIds.size()));
            purchases.add(new ItemPurchase(mOrderId++,
                    customerId, generateItemId(), generateQuantity(), generateUnitPrice()));
        }
        return purchases;
    }

    public int generateCustomerId() {
        return mRandom.nextInt(CUSTOMERS_COUNT);
    }

    public int generateItemId() {
        return mRandom.nextInt(1000) + 1;
    }

    public int generateQuantity() {
        return mRandom.nextInt(1000) + 1;
    }

    public int generateUnitPrice() {
        return mRandom.nextInt(1000) + 1;
    }

    public int generateRegionId() {
        return mRandom.nextInt(REGIONS_COUNT) + 1;
    }
}
